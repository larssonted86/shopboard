## About Shopboard

Shopboard is a board style shopping list where you can sort items in to lists/categories to easier find them and with one click send them to the shopping list. <b>
Or use it as a packing list, add the items you want to pack and when they are packed send them to the packed list with one click!<b>
You can easily change the name of the lists to fit your usage!
